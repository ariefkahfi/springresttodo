package com.spring.todo.config;

import com.spring.todo.beans.MySpringAppContext;
import com.spring.todo.beans.MySpringWebContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class MyWebConfig extends AbstractAnnotationConfigDispatcherServletInitializer {



    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                MySpringAppContext.class
        };
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                MySpringWebContext.class
        };
    }

    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
