package com.spring.todo.entity;


import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.DateFormat;
import java.util.Date;

@Entity
@Table(name = "todo")
public class Todo {


    @Id
    @Column(name = "todo_id",length = 5)
    @JsonProperty("todo_id")
    private String todoId;

    @Column(name = "todo_name",nullable = false)
    @JsonProperty("todo_name")
    private String todoName;

    @Column(name = "todo_desc",nullable = false)
    @JsonProperty("todo_desc")
    private String todoDesc;

    @Column(name = "todo_deadline",nullable = false)
    @JsonProperty("todo_deadline")
    private String todoDeadline;

    @Column(name = "todo_expired",nullable = false,columnDefinition = "TINYINT")
    @JsonProperty("todo_expired")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean todoExpired;





    @ManyToOne
    @JoinColumn(name = "username")
    @JsonIgnore
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTodoId() {
        return todoId;
    }

    public void setTodoId(String todoId) {
        this.todoId = todoId;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoDesc() {
        return todoDesc;
    }

    public void setTodoDesc(String todoDesc) {
        this.todoDesc = todoDesc;
    }

    public String getTodoDeadline() {
        return todoDeadline;
    }

    public void setTodoDeadline(String todoDeadline) {
        this.todoDeadline = todoDeadline;
    }

    public Boolean isTodoExpired() {
        return todoExpired;
    }

    public void setTodoExpired(Boolean todoExpired) {
        this.todoExpired = todoExpired;
    }
}
