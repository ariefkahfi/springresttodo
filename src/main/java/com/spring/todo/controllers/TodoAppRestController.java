package com.spring.todo.controllers;


import com.spring.todo.entity.Todo;
import com.spring.todo.entity.User;
import com.spring.todo.models.NotifyBody;
import com.spring.todo.models.PostTodoBody;
import com.spring.todo.service.HttpNotificationService;
import com.spring.todo.service.TodoService;
import com.spring.todo.utils.MapWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TodoAppRestController {

    @Autowired
    private TodoService todoService;
    @Autowired
    private HttpNotificationService notificationService;

    private final Logger LOGGER = Logger.getLogger(TodoAppRestController.class.getName());


    @DeleteMapping(value="/todo/{todoId}/{username}")
    public ResponseEntity deleteTodoById(@PathVariable("todoId") String tId, @PathVariable("username") String username) {
    	try {
    		todoService.deleteByIdWithUsername(tId,username );
    		return ResponseEntity
    				.status(200)
    				.body(
    						MapWriter.writeData(200, "OK")
    				);
    	}catch (Exception e) {
			return ResponseEntity
					.status(500)
					.body(
							MapWriter.writeData(500, "ERR_INTERNAL_SERVER")
					);
		}
    }


    @GetMapping(value = "/todo/{todoId}")
    public ResponseEntity getTodoById(@PathVariable("todoId") String tId) {
        Todo tTodo = todoService.findById(tId);

        if (tTodo == null) {
            return ResponseEntity
                        .status(404)
                        .body("NOT_FOUND");
        }

        return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,tTodo)
                    );
    }


    @PostMapping(value = "/user/auth")
    public ResponseEntity authPostUser(@RequestBody User u){
        try {
            User authUser = todoService.findByUsernameAndPassword(u.getUsername(),u.getPassword());
            if(authUser == null) throw  new Exception("Error authenticate user");

            return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,"OK")
                    );
        }catch (Exception ex){
            LOGGER.info(ex.getMessage());
            ex.printStackTrace();
            return ResponseEntity
                    .status(500).body(
                            MapWriter.writeData(500,"CANNOT_PROCEED_THIS_REQUEST")
                    );
        }
    }

    @GetMapping(value = "/user/{username}")
    public ResponseEntity getTodosByUsername(@PathVariable("username")String uname){
        try {
            List<Todo> todoList = todoService.getTodosByUser(uname);
            return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,todoList)
                    );
        }catch (Exception ex){
            LOGGER.info(ex.getMessage());
            ex.printStackTrace();
            return ResponseEntity
                    .status(500).body(
                            MapWriter.writeData(500,"CANNOT_PROCEED_THIS_REQUEST")
                    );
        }
    }

    @PostMapping(value = "/user")
    public ResponseEntity postUser(@RequestBody User u){
        try {
            todoService.saveUser(u);
            return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,"OK")
                    );
        }catch (Exception ex){
            LOGGER.info(ex.getMessage());
            ex.printStackTrace();
            return ResponseEntity
                    .status(500).body(
                            MapWriter.writeData(500,"CANNOT_PROCEED_THIS_REQUEST")
                    );
        }
    }

    @GetMapping(value = "/todo/expired/{todoId}")
    public ResponseEntity expiredTodo(@PathVariable("todoId") String tId){
        try {
            todoService.updateToExpired(tId , true);
            return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,"OK")
                    );
        }catch (Exception ex){
            LOGGER.info(ex.getMessage());
            ex.printStackTrace();
            return ResponseEntity
                    .status(500).body(
                            MapWriter.writeData(500,"CANNOT_PROCEED_THIS_REQUEST")
                    );
        }
    }

    @PostMapping(value = "/todo")
    public ResponseEntity postTodo(@RequestBody PostTodoBody todoBody)  {
        Todo mTodo = new Todo();
        mTodo.setTodoName(todoBody.getTodoName());
        mTodo.setTodoDesc(todoBody.getTodoDesc());
        mTodo.setTodoDeadline(todoBody.getTodoDeadline());
        mTodo.setTodoExpired(false);
        mTodo.setTodoId(UUID.randomUUID().toString().substring(0,5));
        try{
            todoService.newTodoForUser(todoBody.getUsername() , mTodo);

            String todoDeadline = mTodo.getTodoDeadline();
            String [] splitTodoDeadline = todoDeadline.split("-");

            int yyyy = Integer.parseInt(splitTodoDeadline[0]);
            int MM = Integer.parseInt(splitTodoDeadline[1]);
            int dd = Integer.parseInt(splitTodoDeadline[2]);

            NotifyBody notifyBody = new NotifyBody(todoBody.getUsername() , mTodo.getTodoId() , yyyy , MM , dd);
            ResponseEntity<String> stringResponseEntity = notificationService.requestNotify(notifyBody);

            LOGGER.info(stringResponseEntity.getBody());

            return ResponseEntity
                    .status(200)
                    .body(
                            MapWriter.writeData(200,"OK")
                    );
        }catch (Exception ex){
            LOGGER.info(ex.getMessage());
            return ResponseEntity
                    .status(500).body(
                            MapWriter.writeData(500,"CANNOT_PROCEED_THIS_REQUEST")
                    );
        }
    }
}

