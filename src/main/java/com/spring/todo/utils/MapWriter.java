package com.spring.todo.utils;

import java.util.HashMap;
import java.util.Map;

public class MapWriter {


    public static Map<String,Object> writeMap(MapCallback  mapCallback){
        return mapCallback.writeValue();
    }

    public static Map<String,Object> writeData(int statusCode , Object data) {
        Map<String,Object> m = new HashMap<String, Object>();
        m.put("code",statusCode);
        m.put("data",data);
        return m;
    }

}
