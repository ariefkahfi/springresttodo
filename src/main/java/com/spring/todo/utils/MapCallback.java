package com.spring.todo.utils;

import java.util.Map;

public interface MapCallback<T> {
    Map<String,Object> writeValue();
}
