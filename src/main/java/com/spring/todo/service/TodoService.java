package com.spring.todo.service;


import com.spring.todo.dao.TodoDAO;
import com.spring.todo.dao.UserDAO;
import com.spring.todo.entity.Todo;
import com.spring.todo.entity.User;
import org.hibernate.Hibernate;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TodoService {

    private TodoDAO todoDAO;
    private UserDAO userDAO;
    private StrongPasswordEncryptor passwordEncryptor;
    private HttpNotificationService notificationService;


    @Autowired
    public TodoService(
            TodoDAO todoDAO ,
            UserDAO userDAO ,
            StrongPasswordEncryptor passwordEncryptor ,
            HttpNotificationService notificationService) {
        this.todoDAO = todoDAO;
        this.userDAO = userDAO;
        this.passwordEncryptor = passwordEncryptor;
        this.notificationService = notificationService;
    }


    @Transactional
    public void save(Todo t) {
        todoDAO.save(t);
    }

    @Transactional(readOnly = true)
    public Todo findById(String tId){
        return todoDAO.findById(tId);
    }


    @Transactional
    public void deleteByIdWithUsername(String tId, String username){
        todoDAO.deleteById(tId);
        notificationService.deleteTodoNotify(tId,username);
    }


    @Transactional
    public void newTodoForUser(String username , Todo newTodo){
        User getUser = userDAO.findByUsername(username);
        todoDAO.save(newTodo);
        newTodo.setUser(getUser);
    }

    @Transactional
    public List<Todo> getTodosByUser(String username){
        User getUser = userDAO.findByUsername(username);
        Hibernate.initialize(getUser.getTodoList());
        return getUser.getTodoList();
    }

    @Transactional
    public void updateToExpired(String tId , boolean newStatus){
        Todo getTodo = todoDAO.findById(tId);
        getTodo.setTodoExpired(newStatus);
    }

    @Transactional
    public void saveUser(User u){
        String currentPassword = u.getPassword();
        String encryptedPassword = passwordEncryptor.encryptPassword(currentPassword);
        u.setPassword(encryptedPassword);
        userDAO.save(u);
    }

    @Transactional(readOnly = true)
    public User findByUsername(String username){
        return userDAO.findByUsername(username);
    }

    @Transactional(readOnly = true)
    public User findByUsernameAndPassword(String username, String password){
        User getUser = userDAO.findByUsername(username);
        boolean isValid= passwordEncryptor.checkPassword(password ,getUser.getPassword());
        if (!isValid) return  null;
        return getUser;
    }

}
