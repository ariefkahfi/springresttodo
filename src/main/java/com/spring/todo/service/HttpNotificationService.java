package com.spring.todo.service;


import java.net.URI;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.spring.todo.models.NotifyBody;

@Service
public class HttpNotificationService {
	private RestTemplate restTemplate = new RestTemplate();


	public void deleteTodoNotify(String todoId, String username){
        restTemplate.delete("http://notif-app:7777/notif-service/api/notify/{todoId}/{username}",todoId,username);
	}
	
	public ResponseEntity<String> requestNotify(NotifyBody nBody) {
		String json = NotifyBody.toJson(nBody.getUsername(), nBody.getTodoId(), nBody.getTodoDeadlineYear(),
				nBody.getTodoDeadlineDay(), nBody.getTodoDeadlineMonth());
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-type", "application/json");
		HttpEntity<String> httpEntity = new HttpEntity<String>(json,headers);
		
		ResponseEntity<String> response = restTemplate.postForEntity(URI.create("http://notif-app:7777/notif-service/api/notify"), httpEntity, String.class);

		return response;
	}
	
}
