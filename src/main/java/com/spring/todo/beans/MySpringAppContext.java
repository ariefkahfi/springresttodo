package com.spring.todo.beans;


import com.spring.todo.entity.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import com.spring.todo.entity.Todo;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:db.properties")
@ComponentScan(basePackages = {"com.spring.todo.dao","com.spring.todo.service"})
public class MySpringAppContext {


    private Environment env;




    @Autowired
    public MySpringAppContext(Environment env){
        this.env = env;
    }


    @Bean
    public DataSource basicDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(env.getProperty("db.url"));
        dataSource.setDriverClassName(env.getProperty("db.driver"));
        dataSource.setUsername(env.getProperty("db.username"));
        dataSource.setPassword(env.getProperty("db.pass"));
        return dataSource;
    }


    @Bean
    public StrongPasswordEncryptor passwordEncryptor(){
        return new StrongPasswordEncryptor();
    }
   
    
    @Bean
    public LocalSessionFactoryBean localSessionFactory(){
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(basicDataSource());

        Properties hibernateProps = new Properties();
        hibernateProps.put("hibernate.dialect","org.hibernate.dialect.MySQL57Dialect");
        hibernateProps.put("hibernate.hbm2ddl.auto","update");
        
        sessionFactoryBean.setAnnotatedClasses(
        	Todo.class,
            User.class
        );
        sessionFactoryBean.setHibernateProperties(hibernateProps);

        return sessionFactoryBean;
    }

    @Bean
    public PlatformTransactionManager hibernateTxManager() {
        return new HibernateTransactionManager(localSessionFactory().getObject());
    }

}
