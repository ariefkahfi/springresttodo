package com.spring.todo.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class NotifyBody {


	@SerializedName("username")
	private String username;

	@SerializedName("todo_id")
	private String todoId;
	
	@SerializedName("todo_deadline_year")
	private int todoDeadlineYear;
	
	@SerializedName("todo_deadline_month")
	private int todoDeadlineMonth;
	
	@SerializedName("todo_deadline_day")
	private int todoDeadlineDay;
	
	
	public NotifyBody() {
		
	}
	
	
	public NotifyBody(String username, String todoId, int todoDeadlineYear, int todoDeadlineMonth, int todoDeadlineDay) {
		this.username = username;
		this.todoId = todoId;
		this.todoDeadlineYear = todoDeadlineYear;
		this.todoDeadlineMonth = todoDeadlineMonth;
		this.todoDeadlineDay = todoDeadlineDay;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public void setTodoId(String todoId) {
		this.todoId = todoId;
	}
	public void setTodoDeadlineDay(int todoDeadlineDay) {
		this.todoDeadlineDay = todoDeadlineDay;
	}
	public void setTodoDeadlineMonth(int todoDeadlineMonth) {
		this.todoDeadlineMonth = todoDeadlineMonth;
	}
	public void setTodoDeadlineYear(int todoDeadlineYear) {
		this.todoDeadlineYear = todoDeadlineYear;
	}


	public String getUsername() {
		return username;
	}
	public int getTodoDeadlineDay() {
		return todoDeadlineDay;
	}
	public int getTodoDeadlineMonth() {
		return todoDeadlineMonth;
	}
	public int getTodoDeadlineYear() {
		return todoDeadlineYear;
	}
	public String getTodoId() {
		return todoId;
	}
	
	@Override
	public String toString() {
		return "NotifyBody [todoId=" + todoId + ", todoDeadlineYear=" + todoDeadlineYear + ", todoDeadlineMonth="
				+ todoDeadlineMonth + ", todoDeadlineDay=" + todoDeadlineDay + "]";
	}
	
	
	public static String toJson(String username,String todoId, int todoDeadlineYear , int todoDeadlineDay, int todoDeadlineMonth) {
		NotifyBody nBody = new NotifyBody(username , todoId , todoDeadlineYear , todoDeadlineMonth , todoDeadlineDay);
		Gson gson = new Gson();
		return gson.toJson(nBody);
	}
	
}
