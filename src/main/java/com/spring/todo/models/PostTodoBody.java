package com.spring.todo.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostTodoBody {

    @JsonProperty("username")
    private String username;

    @JsonProperty("todo_name")
    private String todoName;

    @JsonProperty("todo_desc")
    private String todoDesc;

    @JsonProperty("todo_deadline")
    private String todoDeadline;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoDesc() {
        return todoDesc;
    }

    public void setTodoDesc(String todoDesc) {
        this.todoDesc = todoDesc;
    }

    public String getTodoDeadline() {
        return todoDeadline;
    }

    public void setTodoDeadline(String todoDeadline) {
        this.todoDeadline = todoDeadline;
    }
}
