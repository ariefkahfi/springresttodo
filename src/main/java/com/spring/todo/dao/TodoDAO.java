package com.spring.todo.dao;

import com.spring.todo.entity.Todo;

public interface TodoDAO {
    void save(Todo t);
    Todo findById(String tId);
    void deleteById(String tId);
}
