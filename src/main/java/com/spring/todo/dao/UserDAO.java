package com.spring.todo.dao;

import com.spring.todo.entity.User;

public interface UserDAO {
    void save (User u);
    User findByUsername(String username);
    User findByUsernameAndPassword(String username , String password);
}
