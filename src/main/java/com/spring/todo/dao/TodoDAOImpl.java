package com.spring.todo.dao;

import com.spring.todo.entity.Todo;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class TodoDAOImpl implements TodoDAO {


    private SessionFactory sessionFactory;


    @Autowired
    public TodoDAOImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    public void save(Todo t) {
        sessionFactory
                .getCurrentSession()
                .save(t);
    }

    public Todo findById(String tId) {
        return sessionFactory
                .getCurrentSession()
                .get(Todo.class,tId);
    }

    public void deleteById(String tId) {
    	Todo t = findById(tId);
        sessionFactory
                .getCurrentSession()
                .delete(t);
    }


}
