package com.spring.todo.dao;

import com.spring.todo.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO {


    private SessionFactory sessionFactory;

    public UserDAOImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    public void save(User u) {
        sessionFactory
                .getCurrentSession()
                .save(u);
    }

    public User findByUsername(String username) {
        return sessionFactory
                .getCurrentSession()
                .get(User.class,username);
    }

    public User findByUsernameAndPassword(String username, String password) {
        return (User) sessionFactory
                .getCurrentSession()
                .createQuery("FROM User where username = :u and password := p")
                .setParameter("u",username)
                .setParameter("p",password)
                .getSingleResult();
    }
}
