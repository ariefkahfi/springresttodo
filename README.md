## Spring Rest Todo API



##### Base URL (Local)
> http://localhost:8989/RestTodo/api


##### Base URL (Cloud)
> https://ariefkahfi.itpolsri.com/RestTodo/api

#### POST /todo
```
username: string
todo_name: string
todo_desc: string
todo_deadline: string (YYYY-MM-dd)
```

#### POST /user
Content-type: application/json  
```
username: string
password: string
```

#### GET /user/{username}

#### POST /user/auth
```
username: string
password: string
```

#### GET /todo/{todoId}

#### DELETE /todo/{todoId}





