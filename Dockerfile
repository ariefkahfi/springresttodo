FROM tomcat:latest

WORKDIR /SpringRestTodo/

COPY . .

RUN chmod 777 wait-for-it.sh

EXPOSE 8080

CMD ["catalina.sh","run"]
